

import unittest
from main import NoobBot

class TestNoobBot(unittest.TestCase):
    def testOnYourCar(self):
        bot = NoobBot(None, None, None)
        bot.ping = lambda: "Ping"
        yourcar = { "msgType": "yourCar", "data": {
                "name": "Schumacher",
                "color": "red"
                }}
        bot.on_your_car(yourcar['data'])
        self.assertEquals(bot.car_name, "Schumacher")
        self.assertEquals(bot.color, "red")

    def testLapTime(self):
        bot = NoobBot(None, None, None)
        bot.ping = lambda: "testLapTime"
        lapFinished = {"msgType": "lapFinished", "data": {
                "car": {
                    "name": "Schumacher",
                    "color": "red"
                    },
                "lapTime": {
                    "lap": 1,
                    "ticks": 666,
                    "millis": 6660
                    },
                "raceTime": {
                    "laps": 1,
                    "ticks": 666,
                    "millis": 6660
                    },
                "ranking": {
                    "overall": 1,
                    "fastestLap": 1
                    }
                }, "gameId": "OIUHGERJWEOI", "gameTick": 300}
        bot.on_lap_finished(lapFinished['data'])


if __name__ == "__main__":
    unittest.main()
