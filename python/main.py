import json
import socket
import sys
from track import Track
from util import *

# State machine states
UNDEFINED = 0
ENTER_STRAIGHT = 1
ON_STRAIGHT = 2
ON_BRAKING_ZONE = 3
ON_CURVE = 4

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.braking_distance = -1
        self.state = UNDEFINED
        self.current_throttle = 0.0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        self.msg("join", {"name": self.name,
                          "key": self.key})

    def join_race(self, car_count):
        self.msg("joinRace", {"botId": {"name": self.name,
                                        "key": self.key},
                              "carCount": car_count})

    def throttle(self, throttle):
        self.current_throttle = throttle
        self.msg("throttle", throttle)

    def switch_lane(self, direction):
        print("Switching lane to %s (when possible)" % direction)
        self.msg("switchLane", direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_multicar_race(self, car_count):
        self.join_race(car_count)
        self.msg_loop()

    def on_join(self, data):
        print("Joined a quick race")
        self.ping()

    def on_join_race(self, data):
        print("Joined a multicar race")
        self.ping()

    def on_your_car(self, data):
        save_car_info(self, data)
        print("Received car info: Name %s, Color: %s" % (self.car_name, self.color))
        self.ping()

    def on_game_init(self, data):
        print("Received race info")
        self.track = Track(data['race']['track'])
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)

    def on_car_positions(self, data):
        own_position = get_position(self.color, data)
        piece_index = own_position['piecePosition']['pieceIndex']
        new_throttle = -1
        if self.track.is_straight_piece(piece_index):
            # The current piece that the car is on is a straight piece
            piece_distance = own_position['piecePosition']['inPieceDistance']
            distance_to_turn = self.track.get_distance_to_next_turn(piece_index, piece_distance)
            if self.state == ON_CURVE or self.state == UNDEFINED:
                # We have either just entered straight piece after corner or race has started
                self.state = ENTER_STRAIGHT
                self.braking_distance = 0.17 * distance_to_turn
                print("Braking distance set to %.2f" % self.braking_distance)
                new_throttle = 1.0
            elif (distance_to_turn > self.braking_distance):
                print("Distance to go before braking: %.2f" % (distance_to_turn - self.braking_distance))
                self.state = ON_STRAIGHT
                new_throttle = 1.0
            else:
               print("Braking, distance to turn: %.2f" % distance_to_turn)
               self.state = ON_BRAKING_ZONE
               new_throttle = 0.0
        else:
            # The current piece that the car is on is a corner piece
            print("Driving on a corner")
            self.state = ON_CURVE
            new_throttle = 0.5

        if self.current_throttle == new_throttle:
            # The requested and current throttle are equal,
            # we can consider changing lane
            direction = get_lane_selection()
            if direction != STRAIGHT:
                self.switch_lane(direction)
            else:
                self.throttle(new_throttle)
        else:
            self.throttle(new_throttle)

    def on_crash(self, data):
        print("A car has crashed")
        self.ping()

    def on_spawn(self, data):
        print("Crashed car has respawned")
        self.ping()

    def on_lap_finished(self, data):
        print("==========\n" + get_lap_time(data) + "\n==========")
        self.ping()

    def on_finish(self, data):
        print("A car has finished the race")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_tournament_end(self, data):
        print("Tournament ended")
        self.ping()

    def on_dnf(self, data):
        print("A car has been disqualified")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def __pretty_print(self, data):
        print(json.dumps(data, sort_keys=False, indent=2))

    def __str__(self):
        retval = " Name: " + self.car_name
        retval += " Color: " + self.color
        return retval

    def msg_loop(self):
        msg_map = {
            'join':          self.on_join,
            'joinRace':      self.on_join_race,
            'yourCar':       self.on_your_car,
            'gameInit':      self.on_game_init,
            'gameStart':     self.on_game_start,
            'carPositions':  self.on_car_positions,
            'crash':         self.on_crash,
            'spawn':         self.on_spawn,
            'lapFinished':   self.on_lap_finished,
            'finish':        self.on_finish,
            'gameEnd':       self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'dnf':           self.on_dnf,
            'error':         self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                try:
                    msg_map[msg_type](data)
                except Exception as e:
                    print("Error: {0}, msg_type {1}".format(e, msg_type))
                    self.ping()
            else:
                print("Unexpected msg_type {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    argument_count = len(sys.argv)
    if argument_count == 5 or argument_count == 6:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        if argument_count == 5:
            bot.run()
        else:
            car_count = int(sys.argv[5])
            print("Multicar race with %d cars" % car_count)
            bot.run_multicar_race(car_count)
    else:
        print("Usage: ./run host port botname botkey [car_count]")
