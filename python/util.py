# Lane selection
STRAIGHT = "Straight"
LEFT = "Left"
RIGHT = "Right"

def save_car_info(self, data):
   self.car_name = data['name']
   self.color = data['color']


def get_lap_time(data):
    retval = "Laptime: %sms " % (data['lapTime']['millis'])
    retval += "Race time: %sms " % (data['raceTime']['millis'])
    return retval

def get_position(car_color, data):
    for index in range(len(data)):
      if data[index]['id']['color'] == car_color:
        return data[index]
    return None

def get_lane_selection():
    # TODO
    return STRAIGHT
