class Track():

    def __init__(self, track_data):
        self.pieces = track_data['pieces']

    def is_straight_piece(self, piece_index):
        return 'length' in self.pieces[piece_index]

    def get_distance_to_next_turn(self, piece_index, piece_distance):
        distance = self.pieces[piece_index]['length'] - piece_distance
        next_piece_index = (piece_index + 1) % len(self.pieces)
        while self.is_straight_piece(next_piece_index):
            distance += self.pieces[next_piece_index]['length']
            next_piece_index = (next_piece_index + 1) % len(self.pieces)
        return distance
